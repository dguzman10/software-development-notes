# What is Software development? #

*Software* - a set of instructions or programs that tell a computer what to do.
It is independent of hardware and makes computers programmable.

Three basic types of software:

	System software - to provide core functions such as operating systems, disk management,
	utilities, hardware management and other operational necessities.
	
	Programming software - to give programmers tools such as text editors, compilers, linkers, debuggers
	and other tools to create code.
	
	Application Software - to help users perform tasks. Office producitivy suites, data management software, media players,
	and video games are examples. 
	
	Possible Fourth Type... Embedded Software - used to control machines and devices not typically considered computers
	such as cars, industrial robots, telecommuncations networks and more. 

*Programmers/Coders*  - write source code to program computers to do specific tasks like merging databases, processing online orders,
routing communications, conducting searches or displaying text and graphics.

Typically interpret instructions from software developers and engineers.

*Software Engineers* - Apply engineering principles to build software and systems to solve problems. They use modeling language
and other tools to devise solutions that can often be applied to problems in a general way, as opposed to merely solving for a specific instance or client.

*Software Developers* - A less formal role than enginners and can be closely involved with specific project areas including writing code. 
They drive the software development lifecycle - including working across functional teams to transform requirements into features, managing development teams and process,
and conducting software testing and maintenance.

# Steps in the software development process #

- Selecting a methodology (Agile development, DevOps, Rapid Application Development (RAD), Scaled Agile Framework, Waterfall)
- Gathering Requirements : to understand and document what is required by users and other stakeholders 
- Choosing or building an architecture : the underlying structure within which the software will operate
- Developing a design - Solutions to the problems presented by the requirements, using process models and storyboards.
- Building a model - using a modeling tool with a modeling language like SysML or UML to conduct early validation, prototyping and simulation of the design
- Constructing code : involves peer and team review to eliminate problems early and produce quality software faster
- Testing :  expected scenarios as part of the software design and coding - and conducting performance testing such as loading time
- Managing configuration and defects : establish quality assurance priorities (no bugs) and release criteria to address and track defects (logging and tracing)
- Deploying :  the software ready for use and responding to and resolving user problems
- Migrating data : to the new or updated software from existing applications or data sources 
- Managing and measuring the project - maintaining quality and delivery over the application lifecycle, and to evaluate the development process with models such as CMM.

The steps fit into the ALM (Application Lifecycle Management)

The IBM Engineering Management solution is a superset of ALM:

- Requirements analysis and specification
- Design and development
- Testing
- Deployment
- aintenance and support

Why is software development important?

Software development is important because it is pervasive

What this means is that software development, technology, applications are everywhere and spread very rapidly across the world. 
Tech is constantly evolving and we have to learn to adapt and apply it to our daily lives.

It is evolving so quickly because of how fast we are able to continuously integrate and develop with all of the software tools at our disposal.
Tough competition (disruptive tech) to have the best software creates an increase in the skill level of software enginners and developers.
My favorite example: AMD and Intel

# Key Features of Effective Software Development (*) / Terms #

* Artificial Intelligence (AI) - AI enables software to emulate human decision-making and learning. Using neural networks,
machine learning, natural language processing, etc with the opportunity to offer products and services that will disrupt marketplaces.

* Cloud-native Development - a way of building applications through cloud environments. Consists of discrete, reusable components known as microservices
that are designed to intergrate into any cloud environment. Microservices act as building blocks and are often packaged in containers. Improves application
performance, flexibility and extensibility. 

* Cloud-based development - Using the cloud to improve resource management and cut costs. The cloud is used as a fast, flexible and cost-efficient
IDE or PaaS. Cloud-based development environments support coding, design, intergration, testing and other development functions. Offer access to API's,
microservices, DevOps and other development tools and services.

* Blockchain - A secure, digitally linked ledge that eliminates cost and vulnerablity introduced by parties like banks, regulatory bodies and other intermediaries.
It is transforming businesses by freeing capital, accelerating processes, lowering transaction costs and more. Another disruptive technology.
Also hashgraphs??

* Low Code- Products and/or cloud services for application developmentthat employ visual, declarative techniques instead of programming. 
A development practice that reduceds the need for coding and enables non-coders to build or help build applications quickly at lower cost.

* Analytics - Using API's and cloud based services to guide data exploration, automate predictive analyitcs and create dashboards that deliver new insights and improve decision making.

* Model Based Systems Engineering - software modeling languages in MBSE are used to perform early prototyping, simulation and analysis of software designs for early validation.
Building designs in a model based system helps you to analyze and elaborate project requirements and move rapidly from design to implementation.

* Mobile - creating mobile apps with deep connections to data that enriches and elevates user experiences. 

Agile development - breaks requirements into consumable functions and delivers rapidly on those functions through incremental development. A feedback loop helps find and fix defects as functionality continues to deploy.

Capability Maturity Model (CMM) -  assesses the proficiency of software development processes. It tracks progress from ad hoc actions to defined steps to measured results and optimized processes.

DevOps - a combination of development and operations, is an agile-based approach that brings software development and IT operations together in the design, development, deployment and support of software.

Rapid application development (RAD) - is a non-linear approach that condenses design and code construction into one interconnected step.

Scaled Agile Framework (SAFe) - provides a way to scale agile methodology to a larger software development team or organization.

Waterfall - often considered the traditional software development methodology, is a set of cascading linear steps from planning and requirements gathering through deployment and maintenance.

# Software Development Tools and Solutions #

IBM Engineering Systems Design Rhapsody - A proven solution for modeling and design activities that helps you deliver higher quality softawre and systems faster.

IBM Engineering Workflow Management - Advanced software version control, workspace management, distributed source control and parallel development support for individuals and teams to improve productivity by
automatically tracking changes to artifacts. Enables an unlimited suspend-and-resume feature to handle work interruptions.

IBM Engineering Lifecycle Optimization - Integrated Adapters : provides connections between IBM Engineering LifeCycle Management tools and 3rd party tools like Git, GitLib, and Github for managing version control of software.